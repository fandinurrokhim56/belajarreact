import React, { useState } from "react";
import axios from "axios";
import Modal from "@/Molecules/Modal";
import InputComponent from "@/Molecules/Input";  // Adjust the import based on your structure

const EditProductModal = ({ product, onClose, onProductUpdate }) => {
    const [formValues, setFormValues] = useState({
        name: product.name,
        description: product.description,
        // Add other fields as needed
    });

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [successMessage, setSuccessMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [validationErrors, setValidationErrors] = useState({});

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        setIsSubmitting(true);
        setSuccessMessage("");
        setErrorMessage("");
        setValidationErrors({});

        try {
            const response = await axios.put(
                `/products/${product.id}`,
                formValues
            );

            if (response.status === 200) {
                setSuccessMessage("Product updated successfully!");
                onProductUpdate(response.data.product); // Update parent component state
                setTimeout(() => {
                    onClose(); // Close modal after success
                }, 1000);
            }
        } catch (error) {
            if (error.response) {
                if (error.response.status === 422) {
                    setValidationErrors(error.response.data.errors);
                    setErrorMessage(""); // Clear any general error messages
                } else {
                    setErrorMessage("An error occurred: " + error.response.data.error);
                    setValidationErrors({}); // Clear validation errors
                }
            } else {
                setErrorMessage("An error occurred. Please try again.");
                setValidationErrors({}); // Clear validation errors
            }
            setSuccessMessage(""); // Clear any success messages
        } finally {
            setIsSubmitting(false);
        }
    };

    return (
        <Modal
            show={true} // Pass true to always show modal for editing
            onClose={onClose}
            title="Edit Product"
            type="edit"
            confirmButton="Update"
            handleFormSubmit={handleFormSubmit} // Pass handleFormSubmit to Modal
            isSubmitting={isSubmitting} // Pass isSubmitting to Modal
        >
            {/* Form fields */}
            <div className="mb-4">
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">Name</label>
                <input type="text" id="name" name="name" value={formValues.name} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
            </div>
            <div className="mb-4">
                <label htmlFor="description" className="block text-sm font-medium text-gray-700">Description</label>
                <input type="text" id="description" name="description" value={formValues.description} onChange={handleInputChange} className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
            </div>
            {/* Add other fields as needed */}

            {errorMessage && <div className="text-red-600">{errorMessage}</div>}
            {successMessage && <div className="text-green-600">{successMessage}</div>}
        </Modal>
    );
};

export default EditProductModal;