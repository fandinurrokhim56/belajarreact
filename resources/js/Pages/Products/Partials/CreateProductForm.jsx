import React, { useState } from "react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/react";
import axios from "axios";
import InputComponent from "@/Molecules/Input";

export default function Product({ auth }) {
    const [formValues, setFormValues] = useState({ name: "", description: "" });
    const [successMessage, setSuccessMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [validationErrors, setValidationErrors] = useState({});

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSuccessMessage("");
        setErrorMessage("");
        setValidationErrors({});

        try {
            const response = await axios.post("/products", formValues);

            setSuccessMessage("Product created successfully!");
            if (response.status === 201) {
                // Assuming your backend returns 201 Created for success
                setSuccessMessage("Product created successfully!");
                setTimeout(() => {
                    window.location.href = "/products"; // Redirect after 1 second
                }, 1000);
            }
        } catch (error) {
            if (error.response) {
                if (error.response.status === 422) {
                    setValidationErrors(error.response.data.errors);
                } else {
                    setErrorMessage(
                        "An error occurred: " + error.response.data.error
                    );
                }
            } else {
                setErrorMessage("Error submitting form");
            }
        }
    };

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Create Product
                </h2>
            }
        >
            <Head title="Create Product" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <form
                        onSubmit={handleSubmit}
                        className="max-w-md mx-auto bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
                    >
                        <InputComponent
                            label="Name"
                            type="text"
                            id="name"
                            name="name"
                            placeholder="Enter name"
                            required
                            className="mb-4"
                            value={formValues.name}
                            onChange={handleInputChange}
                            error={
                                validationErrors.name
                                    ? validationErrors.name.join(", ")
                                    : ""
                            }
                        />

                        <InputComponent
                            label="Description"
                            type="text"
                            id="description"
                            name="description"
                            placeholder="Enter description"
                            className="mb-4"
                            value={formValues.description}
                            onChange={handleInputChange}
                            error={
                                validationErrors.description
                                    ? validationErrors.description.join(", ")
                                    : ""
                            }
                        />

                        {errorMessage && (
                            <div className="text-red-600">{errorMessage}</div>
                        )}
                        {successMessage && (
                            <div className="text-green-600">
                                {successMessage}
                            </div>
                        )}

                        <div className="flex items-center justify-between">
                            <button
                                type="submit"
                                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            >
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
