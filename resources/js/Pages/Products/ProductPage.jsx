import React, { useState } from "react";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link } from "@inertiajs/react";
import EditProductModal from "./Partials/EditProductModal";

export default function Products({ auth, products }) {
    const [selectedProduct, setSelectedProduct] = useState(null); // State to hold the selected product
    const [isEditModalOpen, setIsEditModalOpen] = useState(false); // State to manage the edit modal visibility
    const [productList, setProductList] = useState(products); // State to manage the product list

    const handleEditClick = (product) => {
        setSelectedProduct(product); // Set the selected product in state
        setIsEditModalOpen(true); // Open the edit modal
    };

    const handleProductUpdate = (updatedProduct) => {
        setProductList((prevProducts) =>
            prevProducts.map((product) =>
                product.id === updatedProduct.id ? updatedProduct : product
            )
        );
        setIsEditModalOpen(false); // Close the modal after updating
    };

    const handleDelete = (product) => {
        // Handle delete logic here
    };

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Products
                </h2>
            }
        >
            <Head title="Products" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6">
                            <div className="flex justify-end mb-4">
                                <Link
                                    href="/products/create"
                                    className="bg-blue-500 text-white px-4 py-2 rounded mr-2"
                                >
                                    Add Product
                                </Link>
                            </div>

                            <div className="mt-6">
                                {productList.map((product) => (
                                    <div
                                        key={product.id}
                                        className="border p-4 mb-4 rounded-lg"
                                    >
                                        <h3 className="text-lg font-semibold">
                                            {product.name}
                                        </h3>
                                        <p className="text-gray-700">
                                            {product.description}
                                        </p>
                                        <div className="flex justify-end mt-2">
                                            <button
                                                onClick={() =>
                                                    handleEditClick(product)
                                                }
                                            >
                                                Edit
                                            </button>
                                            <button
                                                className="bg-red-500 text-white px-4 py-2 rounded"
                                                onClick={() =>
                                                    handleDelete(product)
                                                }
                                            >
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {isEditModalOpen && (
                <EditProductModal
                    product={selectedProduct}
                    onClose={() => setIsEditModalOpen(false)}
                    onProductUpdate={handleProductUpdate}
                />
            )}
        </AuthenticatedLayout>
    );
}
