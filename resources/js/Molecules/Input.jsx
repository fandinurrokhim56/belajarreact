import React from "react";
import InputLabel from "../Components/InputLabel";
import InputError from "../Components/InputError";

const InputComponent = ({
    label,
    id,
    name,
    className,
    type,
    placeholder,
    value,
    required,
    readonly,
    disabled,
    onChange,
    error,
}) => {
    return (
        <div className={`mb-4 ${className}`}>
            <InputLabel htmlFor={id || name}>{label}</InputLabel>
            <input
                type={type}
                id={id || name}
                name={name}
                placeholder={placeholder}
                value={value || ''}
                onChange={onChange}
                required={required}
                readOnly={readonly}
                disabled={disabled}
                className="form-control mb-3 mb-lg-0 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm w-full sm:text-sm sm:leading-5"
            />
            {error && <InputError message={error} />}
        </div>
    );
}

export default InputComponent;