import React from "react";

const Modal = ({
    children,
    show = false,
    maxWidth = "2xl",
    closeable = true,
    onClose = () => {},
    id = "",
    title = "",
    type = "",
    confirmButton = "Save",
    buttonId = "",
    formId = "",
    formAction = "",
    formMethod = "",
    handleFormSubmit, 
    isSubmitting,
}) => {
    const close = () => {
        if (closeable) {
            onClose();
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (handleFormSubmit) {
            await handleFormSubmit(e);
        }
    };

    const maxWidthClass = {
        sm: "sm:max-w-sm",
        md: "sm:max-w-md",
        lg: "sm:max-w-lg",
        xl: "sm:max-w-xl",
        "2xl": "sm:max-w-2xl",
    }[maxWidth];

    return (
        <div
            className={`fixed inset-0 z-50 overflow-auto flex justify-center items-center bg-black bg-opacity-50 ${
                show ? "block" : "hidden"
            }`}
        >
            <div className="relative bg-white w-full max-w-3xl mx-auto rounded-lg shadow-lg">
                <div className="modal-header px-6 py-4 border-b">
                    <h5 className="modal-title text-xl font-semibold">
                        {title}
                    </h5>
                    <button
                        type="button"
                        className="absolute top-2 right-4 text-gray-500 hover:text-gray-600"
                        onClick={onClose}
                    >
                        <svg
                            className="h-5 w-5"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M5.293 5.293a1 1 0 011.414 0L10 8.586l3.293-3.293a1 1 0 111.414 1.414L11.414 10l3.293 3.293a1 1 0 01-1.414 1.414L10 11.414l-3.293 3.293a1 1 0 01-1.414-1.414L8.586 10 5.293 6.707a1 1 0 010-1.414z"
                                clipRule="evenodd"
                            />
                        </svg>
                    </button>
                </div>
                <div className="modal-body p-6">
                    <form
                        id={formId}
                        className="form"
                        action={formAction}
                        method={formMethod}
                        autoComplete="off"
                        onSubmit={handleSubmit} // Handle form submission
                    >
                        {children}

                        {type === "add" && (
                            <div className="flex justify-end gap-3">
                                <button
                                    type="reset"
                                    className="btn px-10 py-2 font-bold bg-slate-100 rounded-md border"
                                    onClick={onClose}
                                >
                                    Close
                                </button>
                                <button
                                    type="submit"
                                    className="btn px-10 py-2 font-bold bg-blue-400 rounded-md border"
                                    disabled={isSubmitting}
                                >
                                    {isSubmitting ? (
                                        <span className="flex items-center">
                                            <svg
                                                className="animate-spin h-5 w-5 mr-3"
                                                viewBox="0 0 24 24"
                                            >
                                                <circle
                                                    className="opacity-25"
                                                    cx="12"
                                                    cy="12"
                                                    r="10"
                                                    stroke="currentColor"
                                                    strokeWidth="4"
                                                ></circle>
                                                <path
                                                    className="opacity-75"
                                                    fill="currentColor"
                                                    d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                                                ></path>
                                            </svg>
                                            Loading...
                                        </span>
                                    ) : (
                                        confirmButton
                                    )}
                                </button>
                            </div>
                        )}
                        {type === "edit" && (
                            <div className="flex gap-3">
                                <button
                                    type="submit"
                                    className="btn px-10 py-2 font-bold bg-blue-400 rounded-md border"
                                    disabled={isSubmitting}
                                >
                                    {isSubmitting ? (
                                        <span className="flex items-center">
                                            <svg
                                                className="animate-spin h-5 w-5 mr-3"
                                                viewBox="0 0 24 24"
                                            >
                                                <circle
                                                    className="opacity-25"
                                                    cx="12"
                                                    cy="12"
                                                    r="10"
                                                    stroke="currentColor"
                                                    strokeWidth="4"
                                                ></circle>
                                                <path
                                                    className="opacity-75"
                                                    fill="currentColor"
                                                    d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                                                ></path>
                                            </svg>
                                            Loading...
                                        </span>
                                    ) : (
                                        confirmButton
                                    )}
                                </button>
                                <button
                                    type="button"
                                    className="btn px-10 py-2 font-bold bg-slate-100 rounded-md border"
                                    onClick={onClose}
                                >
                                    Close
                                </button>
                            </div>
                        )}
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Modal;
