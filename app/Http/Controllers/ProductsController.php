<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function index()
    {
        return Inertia::render('Products/ProductPage', [
            'products' => Products::latest()->get(),
            'user' => auth()->user(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Products/Partials/CreateProductForm');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:30',
            'description' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        try {
            $product = Products::create($request->all());

            return response()->json([
                'message' => 'Product created successfully!',
                'product' => $product,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'An error occurred while creating the product.',
                'details' => $e->getMessage(),
            ], 500);
        }
    }

    public function update(Request $request, Products $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:30',
            'description' => 'required|string|max:255',
        ]);


        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        try {
            $product->update($request->all());

            return response()->json([
                'message' => 'Product created successfully!',
                'product' => $product,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'An error occurred while creating the product.',
                'details' => $e->getMessage(),
            ], 500);
        }
    }

    public function destroy(Products $Products)
    {
        $Products->delete();

        return redirect()->route('Products.index');
    }
}
